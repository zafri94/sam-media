# README #
* * *
Simple PHP script to get [Air Pollutant Index Data in Malaysia](http://apims.doe.gov.my/public_v2/api_table.html)

### Getting Started ###
* * *
* Make sure you have PHP installed and it is added to your PATH variable
* Clone this repo

### Usage ###
* * *

Open terminal path/to/sam-media and run below code  

```php show_api.php "area = Sungai Petani"```  

or  

```php show_api.php "state = Kedah"```