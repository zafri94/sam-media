<?php
class StringService
{
    /**
     * Remove whitespaces, tab, break lines
     *
     * @param string $string
     * @return string
     */
    public static function removeSpaces($string)
    {
        return preg_replace('/\s+/', '', $string);
    }

    /**
     * Check if a substring is contained in a string
     *
     * @param string $needle
     * @param string $haystack
     * @return bool
     */
    public static function contains($needle, $haystack)
    {
        if ((strpos($needle, $haystack ) !== false)) {
            return true;
        }
        return false;
    }
}
