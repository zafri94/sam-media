<?php

class APIMSService
{

    static $url = "http://apims.doe.gov.my/data/public/CAQM/last24hours.json";

    /**
     * Get JSON data from APIMS
     *
     * @return array
     */
    static function getLast24Hours()
    {
        $ch = curl_init(self::$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $last24hours = json_decode(curl_exec($ch), true);
        curl_close($ch);
        return $last24hours['24hour_api'];
    }
}
