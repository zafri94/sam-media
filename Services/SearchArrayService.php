<?php
require_once "StringService.php";

class SearchArrayService
{
    /**
     * Search APIMS area data
     *
     * @param [string] $area
     * @param [array] $arrays
     * @return array
     */
    static function searchByArea($area, $arrays)
    {
        foreach ($arrays as $array) {
            $searchedArea = $array[1];
            $array[1] = StringService::removeSpaces($array[1]);
            if (strcasecmp($array[1], $area) == 0) {
                // Remove State and Area values
                unset($array[0], $array[1]);
                return [array_slice($array, 12),$searchedArea];
            }
        }
        return null;
    }

    /**
     * Search APIMS state data
     *
     * @param [string] $area
     * @param [array] $arrays
     * @return array
     */
    static function searchByState($state, $arrays)
    {
        $areas = [];
        $searchedStateName = "";
        foreach ($arrays as $array) {
            if (strcasecmp(StringService::removeSpaces($array[0]), $state) == 0) {
                $searchedStateName = $array[0];
                $areas[] =  $array;
            }
        }
        if (count($areas) > 0) {
            $indicator = [];
            $areaNames = [];
            foreach ($areas as $area) {
                // Remove State values
                unset($area[0]);
                $indicator[] = array_slice($area, 13);
                $areaNames[] = $area[1];
            }
            return [$indicator,$areaNames,$searchedStateName];
        }
        return null;
    }
}
