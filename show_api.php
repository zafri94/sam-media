<?php
require_once "Services/SearchArrayService.php";
require_once "Services/APIMSService.php";
require_once "Services/StringService.php";
require_once "Services/Table.php";  // ConsoleTable got from PEAR
// If user does not provide any argument
if (count($argv) <= 1) {
    echo "Please provide variable either 'area' variable or 'state' variable";
    exit();
}
// Remove spaces for easier string search
$argv[1] = StringService::removeSpaces($argv[1]);

// If does not contain '='
if (!StringService::contains($argv[1], "=")) {
    echo "Your argument may be invalid. It should look something like php show_api.php \"area=Sungai Petani\"";
    exit();
}

// Get the key and value of passed arguments
list($key, $val) = explode('=', $argv[1]);
// Get Haze data from the last 24 hours
$hazeDataArr = APIMSService::getLast24Hours();
// Remove unnecessary column table header which is State and Location
unset($hazeDataArr[0][0], $hazeDataArr[0][1]);
// Display time based on API
$times = $hazeDataArr[0];
// Remove table header
unset($hazeDataArr[0]);

if ($key == "state") {
    $states = SearchArrayService::searchByState($val, $hazeDataArr);    
    if ($states === null) {
        echo "Unable to find haze data for $val location. Are you sure you got the name correct?";
        exit();
    }
    echo "\nShowing Air Pollutant Index of : ".$states[2] . " for the last 12 hours\n";
    // Display data from the last 12 hours only
    $headers = array_slice($times, 12);
    // Add Area column
    array_unshift($headers, "Area");
    $tbl = new Console_Table();
    $tbl->setHeaders($headers);
    $count = 0;
    foreach ($states[0] as $state) {
        array_unshift($state, $states[1][$count]);
        $tbl->addRow($state);
        $count++;
    }
    echo $tbl->getTable();
} elseif ($key == "area") {
    $area = SearchArrayService::searchByArea($val, $hazeDataArr);
    if ($area[0] === null) {
        echo "Unable to find haze data for $val location. Are you sure you got the name correct?";
        exit();
    }
    echo "\nShowing Air Pollutant Index of : ".$area[1] . " for the last 12 hours\n";
    $tbl = new Console_Table();
    $tbl->setHeaders(array_slice($times, 12));
    $tbl->addRow($area[0]);
    echo $tbl->getTable();
} else {
    echo "Your argument must be either 'area' OR 'state' ONLY!";
}
